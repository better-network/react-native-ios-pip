require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = package["name"]
  s.version      = package["version"]
  s.summary      = package["description"]
  s.homepage     = "https://bitbucket.org/better-network/react-native-ios-pip"
  s.license      = "MIT"
  s.authors      = { "Orland Karamani" => "orlandokaramani@gmail.com" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://orlandokaramani@bitbucket.org/better-network/react-native-ios-pip.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,c,m,swift}"
  s.requires_arc = true

  s.dependency "React"
  s.dependency "PIPKit"
end

