@objc(Pip)
class Pip: NSObject {

  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  @objc(sampleMethod:numberParameter:callback:)
  func sampleMethod(stringArgument: String, numberArgument: NSNumber, callback: RCTResponseSenderBlock) {
    // TODO: Implement some actually useful functionality
    callback(["numberArgument: \(numberArgument) stringArgument: \(stringArgument)"])
  }
  
}